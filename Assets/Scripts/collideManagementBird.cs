﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class collideManagementBird : MonoBehaviour {

	private GameObject bird;

	// Use this for initialization
	void Start () {
		bird = GameObject.Find("bird1");
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if(collider.name == "bird1")
		{
			bird.GetComponent<endAction>().stop();
			SceneManager.LoadScene("scene4-End");
            //GameObject singleton = GameObject.FindWithTag("singleton");
            //singleton.GetComponent<singleton>().getScoreGameOver();
		}
	}
}
