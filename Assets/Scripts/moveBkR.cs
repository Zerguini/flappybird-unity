﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBkR : MonoBehaviour {

    public float positionRestartX;
	private Vector3 movement;
	private Vector2 size;
	private Vector2 coinsBasGauche;

	void Start () {
		movement = new Vector3(-1,0,0);
		coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
	}
	
	void Update () {

		GetComponent<Rigidbody2D>().velocity = movement;
		size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		
		if (transform.position.x < coinsBasGauche.x - (size.x / 2))
		{
			transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		}


	}
}
