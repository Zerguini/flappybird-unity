using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class singleton : MonoBehaviour
{
 private static singleton instance = null;
 public int score;

 // Game Instance Singleton
 public static singleton Instance
 {
     get
     {
         return instance;
     }
 }

 private void Awake()
 {
     if (instance != null && instance != this)
     {
         Destroy(this.gameObject);
     }

     instance = this;
     DontDestroyOnLoad(this.gameObject);
 }

 void Update()
 {
 Debug.Log(score/2);
 }

 public void add()
  {
      score += 1;
      Text text = GameObject.FindWithTag("score").GetComponent<Text>();
      text.text = (score/2).ToString();
  }

  public void getScore(){
      Text textGame = GameObject.FindWithTag("scoreEnd").GetComponent<Text>();
      textGame.text = (score/2).ToString();
  }
}