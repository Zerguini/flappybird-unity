﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// clickButton
struct clickButton_t74693524;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.String
struct String_t;
// clickbuttonMenu
struct clickbuttonMenu_t4008959861;
// collideManagementBird
struct collideManagementBird_t3707204850;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Object
struct Object_t1021602117;
// endAction
struct endAction_t2228375231;
// collideManagementScore
struct collideManagementScore_t2073094137;
// singleton
struct singleton_t1734329981;
// touchAction
struct touchAction_t352115121;
// moveBk
struct moveBk_t75233466;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Transform
struct Transform_t3275118058;
// moveBkR
struct moveBkR_t1969537132;
// movePipes
struct movePipes_t2252709926;
// scoreFinal
struct scoreFinal_t4052325966;
// UnityEngine.UI.Text
struct Text_t356221433;
// wait2Sec
struct wait2Sec_t1458947030;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.UI.FontData
struct FontData_t2614388407;
// UnityEngine.TextGenerator
struct TextGenerator_t647235000;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;

extern Il2CppCodeGenString* _stringLiteral3724287796;
extern const uint32_t clickButton_onClick_m229493892_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral275778952;
extern const uint32_t clickbuttonMenu_onClick_m1485907459_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2078989324;
extern const uint32_t collideManagementBird_Start_m2554352525_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisendAction_t2228375231_m836726786_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1282467590;
extern const uint32_t collideManagementBird_OnTriggerEnter2D_m3547080661_MetadataUsageId;
extern const uint32_t collideManagementScore_Start_m2481720248_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_Tissingleton_t1734329981_m4058690648_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2462408499;
extern const uint32_t collideManagementScore_OnTriggerEnter2D_m3521520416_MetadataUsageId;
extern const uint32_t endAction_Start_m4143153114_MetadataUsageId;
extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TistouchAction_t352115121_m530624136_RuntimeMethod_var;
extern const uint32_t endAction_stop_m1194724400_MetadataUsageId;
extern RuntimeClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t moveBk_Start_m1056386459_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var;
extern const uint32_t moveBk_Update_m1169768776_MetadataUsageId;
extern const uint32_t moveBkR_Start_m2597189105_MetadataUsageId;
extern const uint32_t moveBkR_Update_m1881610230_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_RuntimeMethod_var;
extern const uint32_t movePipes_Start_m3440442861_MetadataUsageId;
extern const uint32_t movePipes_Update_m1687050344_MetadataUsageId;
extern const uint32_t scoreFinal_Start_m2515337775_MetadataUsageId;
extern RuntimeClass* singleton_t1734329981_il2cpp_TypeInfo_var;
extern const uint32_t singleton_get_Instance_m3855149394_MetadataUsageId;
extern const uint32_t singleton_Awake_m2613295545_MetadataUsageId;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t singleton_Update_m3601494109_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2247021248;
extern const uint32_t singleton_add_m1063938385_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3293534843;
extern const uint32_t singleton_getScore_m3135182712_MetadataUsageId;
extern const uint32_t singleton__cctor_m1916394351_MetadataUsageId;
extern RuntimeClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1144830560;
extern const uint32_t touchAction_Update_m2574958869_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4038778778;
extern const uint32_t wait2Sec_Update_m1559094564_MetadataUsageId;
extern const uint32_t wait2Sec_goScene2_m397741991_MetadataUsageId;



#ifndef U3CMODULEU3E_T3783534233_H
#define U3CMODULEU3E_T3783534233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534233 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534233_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef BOUNDS_T3033363703_H
#define BOUNDS_T3033363703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3033363703 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t2243707580  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t2243707580  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Center_0)); }
	inline Vector3_t2243707580  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t2243707580 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t2243707580  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Extents_1)); }
	inline Vector3_t2243707580  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t2243707580 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t2243707580  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3033363703_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef GAMEOBJECT_T1756533147_H
#define GAMEOBJECT_T1756533147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1756533147  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1756533147_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef RIGIDBODY2D_T502193897_H
#define RIGIDBODY2D_T502193897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t502193897  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T502193897_H
#ifndef RENDERER_T257310565_H
#define RENDERER_T257310565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t257310565  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T257310565_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef CAMERA_T189460977_H
#define CAMERA_T189460977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t189460977  : public Behaviour_t955675639
{
public:

public:
};

struct Camera_t189460977_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t834278767 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t834278767 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t834278767 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t834278767 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t834278767 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t834278767 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t834278767 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t834278767 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t834278767 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t834278767 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t834278767 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t834278767 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T189460977_H
#ifndef SPRITERENDERER_T1209076198_H
#define SPRITERENDERER_T1209076198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t1209076198  : public Renderer_t257310565
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T1209076198_H
#ifndef COLLIDER2D_T646061738_H
#define COLLIDER2D_T646061738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t646061738  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T646061738_H
#ifndef CLICKBUTTON_T74693524_H
#define CLICKBUTTON_T74693524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// clickButton
struct  clickButton_t74693524  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKBUTTON_T74693524_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef COLLIDEMANAGEMENTSCORE_T2073094137_H
#define COLLIDEMANAGEMENTSCORE_T2073094137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// collideManagementScore
struct  collideManagementScore_t2073094137  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject collideManagementScore::bird
	GameObject_t1756533147 * ___bird_2;

public:
	inline static int32_t get_offset_of_bird_2() { return static_cast<int32_t>(offsetof(collideManagementScore_t2073094137, ___bird_2)); }
	inline GameObject_t1756533147 * get_bird_2() const { return ___bird_2; }
	inline GameObject_t1756533147 ** get_address_of_bird_2() { return &___bird_2; }
	inline void set_bird_2(GameObject_t1756533147 * value)
	{
		___bird_2 = value;
		Il2CppCodeGenWriteBarrier((&___bird_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDEMANAGEMENTSCORE_T2073094137_H
#ifndef WAIT2SEC_T1458947030_H
#define WAIT2SEC_T1458947030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// wait2Sec
struct  wait2Sec_t1458947030  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAIT2SEC_T1458947030_H
#ifndef COLLIDEMANAGEMENTBIRD_T3707204850_H
#define COLLIDEMANAGEMENTBIRD_T3707204850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// collideManagementBird
struct  collideManagementBird_t3707204850  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject collideManagementBird::bird
	GameObject_t1756533147 * ___bird_2;

public:
	inline static int32_t get_offset_of_bird_2() { return static_cast<int32_t>(offsetof(collideManagementBird_t3707204850, ___bird_2)); }
	inline GameObject_t1756533147 * get_bird_2() const { return ___bird_2; }
	inline GameObject_t1756533147 ** get_address_of_bird_2() { return &___bird_2; }
	inline void set_bird_2(GameObject_t1756533147 * value)
	{
		___bird_2 = value;
		Il2CppCodeGenWriteBarrier((&___bird_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDEMANAGEMENTBIRD_T3707204850_H
#ifndef ENDACTION_T2228375231_H
#define ENDACTION_T2228375231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// endAction
struct  endAction_t2228375231  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject endAction::bird
	GameObject_t1756533147 * ___bird_2;

public:
	inline static int32_t get_offset_of_bird_2() { return static_cast<int32_t>(offsetof(endAction_t2228375231, ___bird_2)); }
	inline GameObject_t1756533147 * get_bird_2() const { return ___bird_2; }
	inline GameObject_t1756533147 ** get_address_of_bird_2() { return &___bird_2; }
	inline void set_bird_2(GameObject_t1756533147 * value)
	{
		___bird_2 = value;
		Il2CppCodeGenWriteBarrier((&___bird_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDACTION_T2228375231_H
#ifndef MOVEBK_T75233466_H
#define MOVEBK_T75233466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// moveBk
struct  moveBk_t75233466  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 moveBk::positionRestartX
	int32_t ___positionRestartX_2;
	// UnityEngine.Vector3 moveBk::movement
	Vector3_t2243707580  ___movement_3;
	// UnityEngine.Vector2 moveBk::size
	Vector2_t2243707579  ___size_4;
	// UnityEngine.Vector2 moveBk::coinsBasGauche
	Vector2_t2243707579  ___coinsBasGauche_5;

public:
	inline static int32_t get_offset_of_positionRestartX_2() { return static_cast<int32_t>(offsetof(moveBk_t75233466, ___positionRestartX_2)); }
	inline int32_t get_positionRestartX_2() const { return ___positionRestartX_2; }
	inline int32_t* get_address_of_positionRestartX_2() { return &___positionRestartX_2; }
	inline void set_positionRestartX_2(int32_t value)
	{
		___positionRestartX_2 = value;
	}

	inline static int32_t get_offset_of_movement_3() { return static_cast<int32_t>(offsetof(moveBk_t75233466, ___movement_3)); }
	inline Vector3_t2243707580  get_movement_3() const { return ___movement_3; }
	inline Vector3_t2243707580 * get_address_of_movement_3() { return &___movement_3; }
	inline void set_movement_3(Vector3_t2243707580  value)
	{
		___movement_3 = value;
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(moveBk_t75233466, ___size_4)); }
	inline Vector2_t2243707579  get_size_4() const { return ___size_4; }
	inline Vector2_t2243707579 * get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(Vector2_t2243707579  value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_coinsBasGauche_5() { return static_cast<int32_t>(offsetof(moveBk_t75233466, ___coinsBasGauche_5)); }
	inline Vector2_t2243707579  get_coinsBasGauche_5() const { return ___coinsBasGauche_5; }
	inline Vector2_t2243707579 * get_address_of_coinsBasGauche_5() { return &___coinsBasGauche_5; }
	inline void set_coinsBasGauche_5(Vector2_t2243707579  value)
	{
		___coinsBasGauche_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEBK_T75233466_H
#ifndef TOUCHACTION_T352115121_H
#define TOUCHACTION_T352115121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// touchAction
struct  touchAction_t352115121  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHACTION_T352115121_H
#ifndef SINGLETON_T1734329981_H
#define SINGLETON_T1734329981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// singleton
struct  singleton_t1734329981  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 singleton::score
	int32_t ___score_3;

public:
	inline static int32_t get_offset_of_score_3() { return static_cast<int32_t>(offsetof(singleton_t1734329981, ___score_3)); }
	inline int32_t get_score_3() const { return ___score_3; }
	inline int32_t* get_address_of_score_3() { return &___score_3; }
	inline void set_score_3(int32_t value)
	{
		___score_3 = value;
	}
};

struct singleton_t1734329981_StaticFields
{
public:
	// singleton singleton::instance
	singleton_t1734329981 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(singleton_t1734329981_StaticFields, ___instance_2)); }
	inline singleton_t1734329981 * get_instance_2() const { return ___instance_2; }
	inline singleton_t1734329981 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(singleton_t1734329981 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_T1734329981_H
#ifndef CLICKBUTTONMENU_T4008959861_H
#define CLICKBUTTONMENU_T4008959861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// clickbuttonMenu
struct  clickbuttonMenu_t4008959861  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKBUTTONMENU_T4008959861_H
#ifndef MOVEBKR_T1969537132_H
#define MOVEBKR_T1969537132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// moveBkR
struct  moveBkR_t1969537132  : public MonoBehaviour_t1158329972
{
public:
	// System.Single moveBkR::positionRestartX
	float ___positionRestartX_2;
	// UnityEngine.Vector3 moveBkR::movement
	Vector3_t2243707580  ___movement_3;
	// UnityEngine.Vector2 moveBkR::size
	Vector2_t2243707579  ___size_4;
	// UnityEngine.Vector2 moveBkR::coinsBasGauche
	Vector2_t2243707579  ___coinsBasGauche_5;

public:
	inline static int32_t get_offset_of_positionRestartX_2() { return static_cast<int32_t>(offsetof(moveBkR_t1969537132, ___positionRestartX_2)); }
	inline float get_positionRestartX_2() const { return ___positionRestartX_2; }
	inline float* get_address_of_positionRestartX_2() { return &___positionRestartX_2; }
	inline void set_positionRestartX_2(float value)
	{
		___positionRestartX_2 = value;
	}

	inline static int32_t get_offset_of_movement_3() { return static_cast<int32_t>(offsetof(moveBkR_t1969537132, ___movement_3)); }
	inline Vector3_t2243707580  get_movement_3() const { return ___movement_3; }
	inline Vector3_t2243707580 * get_address_of_movement_3() { return &___movement_3; }
	inline void set_movement_3(Vector3_t2243707580  value)
	{
		___movement_3 = value;
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(moveBkR_t1969537132, ___size_4)); }
	inline Vector2_t2243707579  get_size_4() const { return ___size_4; }
	inline Vector2_t2243707579 * get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(Vector2_t2243707579  value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_coinsBasGauche_5() { return static_cast<int32_t>(offsetof(moveBkR_t1969537132, ___coinsBasGauche_5)); }
	inline Vector2_t2243707579  get_coinsBasGauche_5() const { return ___coinsBasGauche_5; }
	inline Vector2_t2243707579 * get_address_of_coinsBasGauche_5() { return &___coinsBasGauche_5; }
	inline void set_coinsBasGauche_5(Vector2_t2243707579  value)
	{
		___coinsBasGauche_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEBKR_T1969537132_H
#ifndef MOVEPIPES_T2252709926_H
#define MOVEPIPES_T2252709926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// movePipes
struct  movePipes_t2252709926  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 movePipes::movement
	Vector2_t2243707579  ___movement_2;
	// UnityEngine.Vector2 movePipes::siz
	Vector2_t2243707579  ___siz_3;
	// UnityEngine.Vector2 movePipes::position
	Vector2_t2243707579  ___position_4;
	// UnityEngine.GameObject movePipes::pipe1Up
	GameObject_t1756533147 * ___pipe1Up_5;
	// UnityEngine.GameObject movePipes::pipe1Down
	GameObject_t1756533147 * ___pipe1Down_6;
	// UnityEngine.GameObject movePipes::box1
	GameObject_t1756533147 * ___box1_7;
	// UnityEngine.Transform movePipes::pipe1UpOriginalTransform
	Transform_t3275118058 * ___pipe1UpOriginalTransform_8;
	// UnityEngine.Transform movePipes::pipe1DownOriginalTransform
	Transform_t3275118058 * ___pipe1DownOriginalTransform_9;
	// UnityEngine.Transform movePipes::box1OriginalTransform
	Transform_t3275118058 * ___box1OriginalTransform_10;
	// UnityEngine.Vector2 movePipes::leftBottomCameraBorder
	Vector2_t2243707579  ___leftBottomCameraBorder_11;
	// UnityEngine.Vector2 movePipes::rightBottomCameraBorder
	Vector2_t2243707579  ___rightBottomCameraBorder_12;

public:
	inline static int32_t get_offset_of_movement_2() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___movement_2)); }
	inline Vector2_t2243707579  get_movement_2() const { return ___movement_2; }
	inline Vector2_t2243707579 * get_address_of_movement_2() { return &___movement_2; }
	inline void set_movement_2(Vector2_t2243707579  value)
	{
		___movement_2 = value;
	}

	inline static int32_t get_offset_of_siz_3() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___siz_3)); }
	inline Vector2_t2243707579  get_siz_3() const { return ___siz_3; }
	inline Vector2_t2243707579 * get_address_of_siz_3() { return &___siz_3; }
	inline void set_siz_3(Vector2_t2243707579  value)
	{
		___siz_3 = value;
	}

	inline static int32_t get_offset_of_position_4() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___position_4)); }
	inline Vector2_t2243707579  get_position_4() const { return ___position_4; }
	inline Vector2_t2243707579 * get_address_of_position_4() { return &___position_4; }
	inline void set_position_4(Vector2_t2243707579  value)
	{
		___position_4 = value;
	}

	inline static int32_t get_offset_of_pipe1Up_5() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___pipe1Up_5)); }
	inline GameObject_t1756533147 * get_pipe1Up_5() const { return ___pipe1Up_5; }
	inline GameObject_t1756533147 ** get_address_of_pipe1Up_5() { return &___pipe1Up_5; }
	inline void set_pipe1Up_5(GameObject_t1756533147 * value)
	{
		___pipe1Up_5 = value;
		Il2CppCodeGenWriteBarrier((&___pipe1Up_5), value);
	}

	inline static int32_t get_offset_of_pipe1Down_6() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___pipe1Down_6)); }
	inline GameObject_t1756533147 * get_pipe1Down_6() const { return ___pipe1Down_6; }
	inline GameObject_t1756533147 ** get_address_of_pipe1Down_6() { return &___pipe1Down_6; }
	inline void set_pipe1Down_6(GameObject_t1756533147 * value)
	{
		___pipe1Down_6 = value;
		Il2CppCodeGenWriteBarrier((&___pipe1Down_6), value);
	}

	inline static int32_t get_offset_of_box1_7() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___box1_7)); }
	inline GameObject_t1756533147 * get_box1_7() const { return ___box1_7; }
	inline GameObject_t1756533147 ** get_address_of_box1_7() { return &___box1_7; }
	inline void set_box1_7(GameObject_t1756533147 * value)
	{
		___box1_7 = value;
		Il2CppCodeGenWriteBarrier((&___box1_7), value);
	}

	inline static int32_t get_offset_of_pipe1UpOriginalTransform_8() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___pipe1UpOriginalTransform_8)); }
	inline Transform_t3275118058 * get_pipe1UpOriginalTransform_8() const { return ___pipe1UpOriginalTransform_8; }
	inline Transform_t3275118058 ** get_address_of_pipe1UpOriginalTransform_8() { return &___pipe1UpOriginalTransform_8; }
	inline void set_pipe1UpOriginalTransform_8(Transform_t3275118058 * value)
	{
		___pipe1UpOriginalTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___pipe1UpOriginalTransform_8), value);
	}

	inline static int32_t get_offset_of_pipe1DownOriginalTransform_9() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___pipe1DownOriginalTransform_9)); }
	inline Transform_t3275118058 * get_pipe1DownOriginalTransform_9() const { return ___pipe1DownOriginalTransform_9; }
	inline Transform_t3275118058 ** get_address_of_pipe1DownOriginalTransform_9() { return &___pipe1DownOriginalTransform_9; }
	inline void set_pipe1DownOriginalTransform_9(Transform_t3275118058 * value)
	{
		___pipe1DownOriginalTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___pipe1DownOriginalTransform_9), value);
	}

	inline static int32_t get_offset_of_box1OriginalTransform_10() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___box1OriginalTransform_10)); }
	inline Transform_t3275118058 * get_box1OriginalTransform_10() const { return ___box1OriginalTransform_10; }
	inline Transform_t3275118058 ** get_address_of_box1OriginalTransform_10() { return &___box1OriginalTransform_10; }
	inline void set_box1OriginalTransform_10(Transform_t3275118058 * value)
	{
		___box1OriginalTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___box1OriginalTransform_10), value);
	}

	inline static int32_t get_offset_of_leftBottomCameraBorder_11() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___leftBottomCameraBorder_11)); }
	inline Vector2_t2243707579  get_leftBottomCameraBorder_11() const { return ___leftBottomCameraBorder_11; }
	inline Vector2_t2243707579 * get_address_of_leftBottomCameraBorder_11() { return &___leftBottomCameraBorder_11; }
	inline void set_leftBottomCameraBorder_11(Vector2_t2243707579  value)
	{
		___leftBottomCameraBorder_11 = value;
	}

	inline static int32_t get_offset_of_rightBottomCameraBorder_12() { return static_cast<int32_t>(offsetof(movePipes_t2252709926, ___rightBottomCameraBorder_12)); }
	inline Vector2_t2243707579  get_rightBottomCameraBorder_12() const { return ___rightBottomCameraBorder_12; }
	inline Vector2_t2243707579 * get_address_of_rightBottomCameraBorder_12() { return &___rightBottomCameraBorder_12; }
	inline void set_rightBottomCameraBorder_12(Vector2_t2243707579  value)
	{
		___rightBottomCameraBorder_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEPIPES_T2252709926_H
#ifndef SCOREFINAL_T4052325966_H
#define SCOREFINAL_T4052325966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// scoreFinal
struct  scoreFinal_t4052325966  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOREFINAL_T4052325966_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef TEXT_T356221433_H
#define TEXT_T356221433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t356221433  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t2614388407 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t647235000 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t647235000 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t3048644023* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_FontData_28)); }
	inline FontData_t2614388407 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t2614388407 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t2614388407 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCache_30)); }
	inline TextGenerator_t647235000 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t647235000 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t647235000 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t647235000 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t3048644023* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t3048644023* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t356221433_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t193706927 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t356221433_StaticFields, ___s_DefaultText_32)); }
	inline Material_t193706927 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t193706927 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t193706927 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T356221433_H


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2724124387_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1825328214 (MonoBehaviour_t1158329972 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m2357316016 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m279709565 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m3369637820 (Object_t1021602117 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<endAction>()
#define GameObject_GetComponent_TisendAction_t2228375231_m836726786(__this, method) ((  endAction_t2228375231 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void endAction::stop()
extern "C"  void endAction_stop_m1194724400 (endAction_t2228375231 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindWithTag(System.String)
extern "C"  GameObject_t1756533147 * GameObject_FindWithTag_m3831463693 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<singleton>()
#define GameObject_GetComponent_Tissingleton_t1734329981_m4058690648(__this, method) ((  singleton_t1734329981 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void singleton::add()
extern "C"  void singleton_add_m1063938385 (singleton_t1734329981 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<touchAction>()
#define GameObject_GetComponent_TistouchAction_t352115121_m530624136(__this, method) ((  touchAction_t352115121 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m3959286051 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1555724485 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m881971336 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_ViewportToWorldPoint_m283095757 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m385881926 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, method) ((  Rigidbody2D_t502193897 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m2161463521 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m2159020946 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t3033363703  Renderer_get_bounds_m2683206870 (Renderer_t257310565 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t2243707580  Bounds_get_size_m3951110137 (Bounds_t3033363703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m3374354972 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m2304215762 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2942701431 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
#define GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(__this, method) ((  Rigidbody2D_t502193897 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m3490276752 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void movePipes::moveToRightPipe()
extern "C"  void movePipes_moveToRightPipe_m1732192707 (movePipes_t2252709926 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m3327624272 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void singleton::getScore()
extern "C"  void singleton_getScore_m3135182712 (singleton_t1734329981 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m3768854296 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2658633409 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m2923680153 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
extern "C"  bool Input_GetKeyDown_m3563899393 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m847450945 (Vector2_t2243707579 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m2201735803 (MonoBehaviour_t1158329972 * __this, String_t* p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void clickButton::.ctor()
extern "C"  void clickButton__ctor_m722205303 (clickButton_t74693524 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void clickButton::onClick()
extern "C"  void clickButton_onClick_m229493892 (clickButton_t74693524 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (clickButton_onClick_m229493892_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("scene3-Game");
		// SceneManager.LoadScene("scene3-Game");
		SceneManager_LoadScene_m2357316016(NULL /*static, unused*/, _stringLiteral3724287796, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void clickbuttonMenu::.ctor()
extern "C"  void clickbuttonMenu__ctor_m2613541442 (clickbuttonMenu_t4008959861 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void clickbuttonMenu::onClick()
extern "C"  void clickbuttonMenu_onClick_m1485907459 (clickbuttonMenu_t4008959861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (clickbuttonMenu_onClick_m1485907459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("scene2-Menu");
		// SceneManager.LoadScene("scene2-Menu");
		SceneManager_LoadScene_m2357316016(NULL /*static, unused*/, _stringLiteral275778952, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void collideManagementBird::.ctor()
extern "C"  void collideManagementBird__ctor_m588491949 (collideManagementBird_t3707204850 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void collideManagementBird::Start()
extern "C"  void collideManagementBird_Start_m2554352525 (collideManagementBird_t3707204850 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (collideManagementBird_Start_m2554352525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// bird = GameObject.Find("bird1");
		// bird = GameObject.Find("bird1");
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2078989324, /*hidden argument*/NULL);
		__this->set_bird_2(L_0);
		// }
		return;
	}
}
// System.Void collideManagementBird::Update()
extern "C"  void collideManagementBird_Update_m4220924752 (collideManagementBird_t3707204850 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void collideManagementBird::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void collideManagementBird_OnTriggerEnter2D_m3547080661 (collideManagementBird_t3707204850 * __this, Collider2D_t646061738 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (collideManagementBird_OnTriggerEnter2D_m3547080661_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(collider.name == "bird1")
		Collider2D_t646061738 * L_0 = ___collider0;
		// if(collider.name == "bird1")
		String_t* L_1 = Object_get_name_m3369637820(L_0, /*hidden argument*/NULL);
		// if(collider.name == "bird1")
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral2078989324, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		// bird.GetComponent<endAction>().stop();
		GameObject_t1756533147 * L_3 = __this->get_bird_2();
		// bird.GetComponent<endAction>().stop();
		endAction_t2228375231 * L_4 = GameObject_GetComponent_TisendAction_t2228375231_m836726786(L_3, /*hidden argument*/GameObject_GetComponent_TisendAction_t2228375231_m836726786_RuntimeMethod_var);
		// bird.GetComponent<endAction>().stop();
		endAction_stop_m1194724400(L_4, /*hidden argument*/NULL);
		// SceneManager.LoadScene("scene4-End");
		// SceneManager.LoadScene("scene4-End");
		SceneManager_LoadScene_m2357316016(NULL /*static, unused*/, _stringLiteral1282467590, /*hidden argument*/NULL);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Void collideManagementScore::.ctor()
extern "C"  void collideManagementScore__ctor_m2074477680 (collideManagementScore_t2073094137 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void collideManagementScore::Start()
extern "C"  void collideManagementScore_Start_m2481720248 (collideManagementScore_t2073094137 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (collideManagementScore_Start_m2481720248_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// bird = GameObject.Find("bird1");
		// bird = GameObject.Find("bird1");
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2078989324, /*hidden argument*/NULL);
		__this->set_bird_2(L_0);
		// }
		return;
	}
}
// System.Void collideManagementScore::Update()
extern "C"  void collideManagementScore_Update_m746086337 (collideManagementScore_t2073094137 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void collideManagementScore::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void collideManagementScore_OnTriggerEnter2D_m3521520416 (collideManagementScore_t2073094137 * __this, Collider2D_t646061738 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (collideManagementScore_OnTriggerEnter2D_m3521520416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		// if(collider.name == "bird1")
		Collider2D_t646061738 * L_0 = ___collider0;
		// if(collider.name == "bird1")
		String_t* L_1 = Object_get_name_m3369637820(L_0, /*hidden argument*/NULL);
		// if(collider.name == "bird1")
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral2078989324, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		// GameObject singleton = GameObject.FindWithTag("singleton");
		// GameObject singleton = GameObject.FindWithTag("singleton");
		GameObject_t1756533147 * L_3 = GameObject_FindWithTag_m3831463693(NULL /*static, unused*/, _stringLiteral2462408499, /*hidden argument*/NULL);
		V_0 = L_3;
		// singleton.GetComponent<singleton>().add();
		GameObject_t1756533147 * L_4 = V_0;
		// singleton.GetComponent<singleton>().add();
		singleton_t1734329981 * L_5 = GameObject_GetComponent_Tissingleton_t1734329981_m4058690648(L_4, /*hidden argument*/GameObject_GetComponent_Tissingleton_t1734329981_m4058690648_RuntimeMethod_var);
		// singleton.GetComponent<singleton>().add();
		singleton_add_m1063938385(L_5, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// }
		return;
	}
}
// System.Void endAction::.ctor()
extern "C"  void endAction__ctor_m3299382198 (endAction_t2228375231 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void endAction::Start()
extern "C"  void endAction_Start_m4143153114 (endAction_t2228375231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (endAction_Start_m4143153114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// bird = GameObject.Find("bird1");
		// bird = GameObject.Find("bird1");
		GameObject_t1756533147 * L_0 = GameObject_Find_m279709565(NULL /*static, unused*/, _stringLiteral2078989324, /*hidden argument*/NULL);
		__this->set_bird_2(L_0);
		// }
		return;
	}
}
// System.Void endAction::Update()
extern "C"  void endAction_Update_m2326922019 (endAction_t2228375231 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void endAction::stop()
extern "C"  void endAction_stop_m1194724400 (endAction_t2228375231 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (endAction_stop_m1194724400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(bird.GetComponent<touchAction>());
		GameObject_t1756533147 * L_0 = __this->get_bird_2();
		// Destroy(bird.GetComponent<touchAction>());
		touchAction_t352115121 * L_1 = GameObject_GetComponent_TistouchAction_t352115121_m530624136(L_0, /*hidden argument*/GameObject_GetComponent_TistouchAction_t352115121_m530624136_RuntimeMethod_var);
		// Destroy(bird.GetComponent<touchAction>());
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void moveBk::.ctor()
extern "C"  void moveBk__ctor_m2747884175 (moveBk_t75233466 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void moveBk::Start()
extern "C"  void moveBk_Start_m1056386459 (moveBk_t75233466 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (moveBk_Start_m1056386459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// movement = new Vector3(-1,0,0);
		// movement = new Vector3(-1,0,0);
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m1555724485((&L_0), (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_movement_3(L_0);
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		Camera_t189460977 * L_1 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m1555724485((&L_2), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		Vector3_t2243707580  L_3 = Camera_ViewportToWorldPoint_m283095757(L_1, L_2, /*hidden argument*/NULL);
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_coinsBasGauche_5(L_4);
		// }
		return;
	}
}
// System.Void moveBk::Update()
extern "C"  void moveBk_Update_m1169768776 (moveBk_t75233466 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (moveBk_Update_m1169768776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Bounds_t3033363703  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		// GetComponent<Rigidbody2D>().velocity = movement;
		// GetComponent<Rigidbody2D>().velocity = movement;
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_RuntimeMethod_var);
		Vector3_t2243707580  L_1 = __this->get_movement_3();
		// GetComponent<Rigidbody2D>().velocity = movement;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// GetComponent<Rigidbody2D>().velocity = movement;
		Rigidbody2D_set_velocity_m2161463521(L_0, L_2, /*hidden argument*/NULL);
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		Vector2_t2243707579 * L_3 = __this->get_address_of_size_4();
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		SpriteRenderer_t1209076198 * L_5 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_4, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		Bounds_t3033363703  L_6 = Renderer_get_bounds_m2683206870(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		Vector3_t2243707580  L_7 = Bounds_get_size_m3951110137((&V_0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_x_1();
		L_3->set_x_0(L_8);
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		Vector2_t2243707579 * L_9 = __this->get_address_of_size_4();
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		SpriteRenderer_t1209076198 * L_11 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_10, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		Bounds_t3033363703  L_12 = Renderer_get_bounds_m2683206870(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		Vector3_t2243707580  L_13 = Bounds_get_size_m3951110137((&V_2), /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_y_2();
		L_9->set_y_1(L_14);
		// if (transform.position.x < coinsBasGauche.x - (size.x / 2))
		// if (transform.position.x < coinsBasGauche.x - (size.x / 2))
		Transform_t3275118058 * L_15 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		// if (transform.position.x < coinsBasGauche.x - (size.x / 2))
		Vector3_t2243707580  L_16 = Transform_get_position_m2304215762(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		float L_17 = (&V_4)->get_x_1();
		Vector2_t2243707579 * L_18 = __this->get_address_of_coinsBasGauche_5();
		float L_19 = L_18->get_x_0();
		Vector2_t2243707579 * L_20 = __this->get_address_of_size_4();
		float L_21 = L_20->get_x_0();
		if ((!(((float)L_17) < ((float)((float)((float)L_19-(float)((float)((float)L_21/(float)(2.0f)))))))))
		{
			goto IL_00e4;
		}
	}
	{
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Transform_t3275118058 * L_22 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		int32_t L_23 = __this->get_positionRestartX_2();
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Transform_t3275118058 * L_24 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Vector3_t2243707580  L_25 = Transform_get_position_m2304215762(L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		float L_26 = (&V_5)->get_y_2();
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Transform_t3275118058 * L_27 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Vector3_t2243707580  L_28 = Transform_get_position_m2304215762(L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		float L_29 = (&V_6)->get_z_3();
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Vector3_t2243707580  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m1555724485((&L_30), (((float)((float)L_23))), L_26, L_29, /*hidden argument*/NULL);
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Transform_set_position_m2942701431(L_22, L_30, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		// }
		return;
	}
}
// System.Void moveBkR::.ctor()
extern "C"  void moveBkR__ctor_m3380836177 (moveBkR_t1969537132 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void moveBkR::Start()
extern "C"  void moveBkR_Start_m2597189105 (moveBkR_t1969537132 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (moveBkR_Start_m2597189105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// movement = new Vector3(-1,0,0);
		// movement = new Vector3(-1,0,0);
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m1555724485((&L_0), (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_movement_3(L_0);
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		Camera_t189460977 * L_1 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m1555724485((&L_2), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		Vector3_t2243707580  L_3 = Camera_ViewportToWorldPoint_m283095757(L_1, L_2, /*hidden argument*/NULL);
		// coinsBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_coinsBasGauche_5(L_4);
		// }
		return;
	}
}
// System.Void moveBkR::Update()
extern "C"  void moveBkR_Update_m1881610230 (moveBkR_t1969537132 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (moveBkR_Update_m1881610230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Bounds_t3033363703  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		// GetComponent<Rigidbody2D>().velocity = movement;
		// GetComponent<Rigidbody2D>().velocity = movement;
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_RuntimeMethod_var);
		Vector3_t2243707580  L_1 = __this->get_movement_3();
		// GetComponent<Rigidbody2D>().velocity = movement;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// GetComponent<Rigidbody2D>().velocity = movement;
		Rigidbody2D_set_velocity_m2161463521(L_0, L_2, /*hidden argument*/NULL);
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		Vector2_t2243707579 * L_3 = __this->get_address_of_size_4();
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		SpriteRenderer_t1209076198 * L_5 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_4, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		Bounds_t3033363703  L_6 = Renderer_get_bounds_m2683206870(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		Vector3_t2243707580  L_7 = Bounds_get_size_m3951110137((&V_0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_x_1();
		L_3->set_x_0(L_8);
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		Vector2_t2243707579 * L_9 = __this->get_address_of_size_4();
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		SpriteRenderer_t1209076198 * L_11 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_10, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		Bounds_t3033363703  L_12 = Renderer_get_bounds_m2683206870(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		// size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;
		Vector3_t2243707580  L_13 = Bounds_get_size_m3951110137((&V_2), /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_y_2();
		L_9->set_y_1(L_14);
		// if (transform.position.x < coinsBasGauche.x - (size.x / 2))
		// if (transform.position.x < coinsBasGauche.x - (size.x / 2))
		Transform_t3275118058 * L_15 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		// if (transform.position.x < coinsBasGauche.x - (size.x / 2))
		Vector3_t2243707580  L_16 = Transform_get_position_m2304215762(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		float L_17 = (&V_4)->get_x_1();
		Vector2_t2243707579 * L_18 = __this->get_address_of_coinsBasGauche_5();
		float L_19 = L_18->get_x_0();
		Vector2_t2243707579 * L_20 = __this->get_address_of_size_4();
		float L_21 = L_20->get_x_0();
		if ((!(((float)L_17) < ((float)((float)((float)L_19-(float)((float)((float)L_21/(float)(2.0f)))))))))
		{
			goto IL_00e3;
		}
	}
	{
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Transform_t3275118058 * L_22 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		float L_23 = __this->get_positionRestartX_2();
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Transform_t3275118058 * L_24 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Vector3_t2243707580  L_25 = Transform_get_position_m2304215762(L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		float L_26 = (&V_5)->get_y_2();
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Transform_t3275118058 * L_27 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Vector3_t2243707580  L_28 = Transform_get_position_m2304215762(L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		float L_29 = (&V_6)->get_z_3();
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Vector3_t2243707580  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m1555724485((&L_30), L_23, L_26, L_29, /*hidden argument*/NULL);
		// transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z);
		Transform_set_position_m2942701431(L_22, L_30, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		// }
		return;
	}
}
// System.Void movePipes::.ctor()
extern "C"  void movePipes__ctor_m2718007885 (movePipes_t2252709926 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void movePipes::Start()
extern "C"  void movePipes_Start_m3440442861 (movePipes_t2252709926 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (movePipes_Start_m3440442861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		// leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		Camera_t189460977 * L_0 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		// leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		Vector3_t2243707580  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m1555724485((&L_1), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		Vector3_t2243707580  L_2 = Camera_ViewportToWorldPoint_m283095757(L_0, L_1, /*hidden argument*/NULL);
		// leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_3 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_leftBottomCameraBorder_11(L_3);
		// rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
		// rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
		Camera_t189460977 * L_4 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		// rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
		Vector3_t2243707580  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m1555724485((&L_5), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
		Vector3_t2243707580  L_6 = Camera_ViewportToWorldPoint_m283095757(L_4, L_5, /*hidden argument*/NULL);
		// rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
		Vector2_t2243707579  L_7 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_rightBottomCameraBorder_12(L_7);
		// pipe1UpOriginalTransform = pipe1Up.GetComponent<Rigidbody2D>().transform;
		GameObject_t1756533147 * L_8 = __this->get_pipe1Up_5();
		// pipe1UpOriginalTransform = pipe1Up.GetComponent<Rigidbody2D>().transform;
		Rigidbody2D_t502193897 * L_9 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_8, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_RuntimeMethod_var);
		// pipe1UpOriginalTransform = pipe1Up.GetComponent<Rigidbody2D>().transform;
		Transform_t3275118058 * L_10 = Component_get_transform_m3374354972(L_9, /*hidden argument*/NULL);
		__this->set_pipe1UpOriginalTransform_8(L_10);
		// pipe1DownOriginalTransform = pipe1Down.GetComponent<Rigidbody2D>().transform;
		GameObject_t1756533147 * L_11 = __this->get_pipe1Down_6();
		// pipe1DownOriginalTransform = pipe1Down.GetComponent<Rigidbody2D>().transform;
		Rigidbody2D_t502193897 * L_12 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_11, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_RuntimeMethod_var);
		// pipe1DownOriginalTransform = pipe1Down.GetComponent<Rigidbody2D>().transform;
		Transform_t3275118058 * L_13 = Component_get_transform_m3374354972(L_12, /*hidden argument*/NULL);
		__this->set_pipe1DownOriginalTransform_9(L_13);
		// box1OriginalTransform = box1.GetComponent<Rigidbody2D>().transform;
		GameObject_t1756533147 * L_14 = __this->get_box1_7();
		// box1OriginalTransform = box1.GetComponent<Rigidbody2D>().transform;
		Rigidbody2D_t502193897 * L_15 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_14, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_RuntimeMethod_var);
		// box1OriginalTransform = box1.GetComponent<Rigidbody2D>().transform;
		Transform_t3275118058 * L_16 = Component_get_transform_m3374354972(L_15, /*hidden argument*/NULL);
		__this->set_box1OriginalTransform_10(L_16);
		// }
		return;
	}
}
// System.Void movePipes::Update()
extern "C"  void movePipes_Update_m1687050344 (movePipes_t2252709926 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (movePipes_Update_m1687050344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Bounds_t3033363703  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		// pipe1Up.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe haut
		GameObject_t1756533147 * L_0 = __this->get_pipe1Up_5();
		// pipe1Up.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe haut
		Rigidbody2D_t502193897 * L_1 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_0, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_RuntimeMethod_var);
		Vector2_t2243707579  L_2 = __this->get_movement_2();
		// pipe1Up.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe haut
		Rigidbody2D_set_velocity_m2161463521(L_1, L_2, /*hidden argument*/NULL);
		// pipe1Down.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe bas
		GameObject_t1756533147 * L_3 = __this->get_pipe1Down_6();
		// pipe1Down.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe bas
		Rigidbody2D_t502193897 * L_4 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_3, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_RuntimeMethod_var);
		Vector2_t2243707579  L_5 = __this->get_movement_2();
		// pipe1Down.GetComponent<Rigidbody2D>().velocity = movement; // Déplacement du pipe bas
		Rigidbody2D_set_velocity_m2161463521(L_4, L_5, /*hidden argument*/NULL);
		// box1.GetComponent<Rigidbody2D>().velocity = movement;
		GameObject_t1756533147 * L_6 = __this->get_box1_7();
		// box1.GetComponent<Rigidbody2D>().velocity = movement;
		Rigidbody2D_t502193897 * L_7 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_6, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_RuntimeMethod_var);
		Vector2_t2243707579  L_8 = __this->get_movement_2();
		// box1.GetComponent<Rigidbody2D>().velocity = movement;
		Rigidbody2D_set_velocity_m2161463521(L_7, L_8, /*hidden argument*/NULL);
		// siz.x = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.x; // Récuperation de la taille d’un pipe
		Vector2_t2243707579 * L_9 = __this->get_address_of_siz_3();
		GameObject_t1756533147 * L_10 = __this->get_pipe1Up_5();
		// siz.x = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.x; // Récuperation de la taille d’un pipe
		SpriteRenderer_t1209076198 * L_11 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_10, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		// siz.x = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.x; // Récuperation de la taille d’un pipe
		Bounds_t3033363703  L_12 = Renderer_get_bounds_m2683206870(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		// siz.x = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.x; // Récuperation de la taille d’un pipe
		Vector3_t2243707580  L_13 = Bounds_get_size_m3951110137((&V_0), /*hidden argument*/NULL);
		V_1 = L_13;
		float L_14 = (&V_1)->get_x_1();
		L_9->set_x_0(L_14);
		// siz.y = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.y; // Suffisant car ils ont la même taille
		Vector2_t2243707579 * L_15 = __this->get_address_of_siz_3();
		GameObject_t1756533147 * L_16 = __this->get_pipe1Up_5();
		// siz.y = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.y; // Suffisant car ils ont la même taille
		SpriteRenderer_t1209076198 * L_17 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_16, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		// siz.y = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.y; // Suffisant car ils ont la même taille
		Bounds_t3033363703  L_18 = Renderer_get_bounds_m2683206870(L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		// siz.y = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.y; // Suffisant car ils ont la même taille
		Vector3_t2243707580  L_19 = Bounds_get_size_m3951110137((&V_2), /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = (&V_3)->get_y_2();
		L_15->set_y_1(L_20);
		// if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe();
		GameObject_t1756533147 * L_21 = __this->get_pipe1Up_5();
		// if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe();
		Transform_t3275118058 * L_22 = GameObject_get_transform_m3490276752(L_21, /*hidden argument*/NULL);
		// if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe();
		Vector3_t2243707580  L_23 = Transform_get_position_m2304215762(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		float L_24 = (&V_4)->get_x_1();
		Vector2_t2243707579 * L_25 = __this->get_address_of_leftBottomCameraBorder_11();
		float L_26 = L_25->get_x_0();
		Vector2_t2243707579 * L_27 = __this->get_address_of_siz_3();
		float L_28 = L_27->get_x_0();
		if ((!(((float)L_24) < ((float)((float)((float)L_26-(float)((float)((float)L_28/(float)(2.0f)))))))))
		{
			goto IL_00da;
		}
	}
	{
		// if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe();
		// if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe();
		movePipes_moveToRightPipe_m1732192707(__this, /*hidden argument*/NULL);
	}

IL_00da:
	{
		// }
		return;
	}
}
// System.Void movePipes::moveToRightPipe()
extern "C"  void movePipes_moveToRightPipe_m1732192707 (movePipes_t2252709926 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t2243707580  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		// float randomY = Random.Range (1,4) - 2; // Tirage aléatoire d’un décalage en Y
		// float randomY = Random.Range (1,4) - 2; // Tirage aléatoire d’un décalage en Y
		int32_t L_0 = Random_Range_m3327624272(NULL /*static, unused*/, 1, 4, /*hidden argument*/NULL);
		V_0 = (((float)((float)((int32_t)((int32_t)L_0-(int32_t)2)))));
		// float posX = rightBottomCameraBorder.x + (siz.x / 2); // Calcul du X du bord droite de l’écran
		Vector2_t2243707579 * L_1 = __this->get_address_of_rightBottomCameraBorder_12();
		float L_2 = L_1->get_x_0();
		Vector2_t2243707579 * L_3 = __this->get_address_of_siz_3();
		float L_4 = L_3->get_x_0();
		V_1 = ((float)((float)L_2+(float)((float)((float)L_4/(float)(2.0f)))));
		// float posY = pipe1UpOriginalTransform.position.y + randomY;
		Transform_t3275118058 * L_5 = __this->get_pipe1UpOriginalTransform_8();
		// float posY = pipe1UpOriginalTransform.position.y + randomY;
		Vector3_t2243707580  L_6 = Transform_get_position_m2304215762(L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = (&V_3)->get_y_2();
		float L_8 = V_0;
		V_2 = ((float)((float)L_7+(float)L_8));
		// Vector3 tmpPos = new Vector3 (posX, posY, pipe1Up.transform.position.z);
		float L_9 = V_1;
		float L_10 = V_2;
		GameObject_t1756533147 * L_11 = __this->get_pipe1Up_5();
		// Vector3 tmpPos = new Vector3 (posX, posY, pipe1Up.transform.position.z);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m3490276752(L_11, /*hidden argument*/NULL);
		// Vector3 tmpPos = new Vector3 (posX, posY, pipe1Up.transform.position.z);
		Vector3_t2243707580  L_13 = Transform_get_position_m2304215762(L_12, /*hidden argument*/NULL);
		V_5 = L_13;
		float L_14 = (&V_5)->get_z_3();
		// Vector3 tmpPos = new Vector3 (posX, posY, pipe1Up.transform.position.z);
		Vector3__ctor_m1555724485((&V_4), L_9, L_10, L_14, /*hidden argument*/NULL);
		// pipe1Up.transform.position = tmpPos;
		GameObject_t1756533147 * L_15 = __this->get_pipe1Up_5();
		// pipe1Up.transform.position = tmpPos;
		Transform_t3275118058 * L_16 = GameObject_get_transform_m3490276752(L_15, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = V_4;
		// pipe1Up.transform.position = tmpPos;
		Transform_set_position_m2942701431(L_16, L_17, /*hidden argument*/NULL);
		// posY = pipe1DownOriginalTransform.position.y + randomY;
		Transform_t3275118058 * L_18 = __this->get_pipe1DownOriginalTransform_9();
		// posY = pipe1DownOriginalTransform.position.y + randomY;
		Vector3_t2243707580  L_19 = Transform_get_position_m2304215762(L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		float L_20 = (&V_6)->get_y_2();
		float L_21 = V_0;
		V_2 = ((float)((float)L_20+(float)L_21));
		// tmpPos = new Vector3 (posX,posY, pipe1Down.transform.position.z);
		float L_22 = V_1;
		float L_23 = V_2;
		GameObject_t1756533147 * L_24 = __this->get_pipe1Down_6();
		// tmpPos = new Vector3 (posX,posY, pipe1Down.transform.position.z);
		Transform_t3275118058 * L_25 = GameObject_get_transform_m3490276752(L_24, /*hidden argument*/NULL);
		// tmpPos = new Vector3 (posX,posY, pipe1Down.transform.position.z);
		Vector3_t2243707580  L_26 = Transform_get_position_m2304215762(L_25, /*hidden argument*/NULL);
		V_7 = L_26;
		float L_27 = (&V_7)->get_z_3();
		// tmpPos = new Vector3 (posX,posY, pipe1Down.transform.position.z);
		Vector3__ctor_m1555724485((&V_4), L_22, L_23, L_27, /*hidden argument*/NULL);
		// pipe1Down.transform.position = tmpPos;
		GameObject_t1756533147 * L_28 = __this->get_pipe1Down_6();
		// pipe1Down.transform.position = tmpPos;
		Transform_t3275118058 * L_29 = GameObject_get_transform_m3490276752(L_28, /*hidden argument*/NULL);
		Vector3_t2243707580  L_30 = V_4;
		// pipe1Down.transform.position = tmpPos;
		Transform_set_position_m2942701431(L_29, L_30, /*hidden argument*/NULL);
		// tmpPos = new Vector3 (posX,box1.transform.position.y, box1.transform.position.z);
		float L_31 = V_1;
		GameObject_t1756533147 * L_32 = __this->get_box1_7();
		// tmpPos = new Vector3 (posX,box1.transform.position.y, box1.transform.position.z);
		Transform_t3275118058 * L_33 = GameObject_get_transform_m3490276752(L_32, /*hidden argument*/NULL);
		// tmpPos = new Vector3 (posX,box1.transform.position.y, box1.transform.position.z);
		Vector3_t2243707580  L_34 = Transform_get_position_m2304215762(L_33, /*hidden argument*/NULL);
		V_8 = L_34;
		float L_35 = (&V_8)->get_y_2();
		GameObject_t1756533147 * L_36 = __this->get_box1_7();
		// tmpPos = new Vector3 (posX,box1.transform.position.y, box1.transform.position.z);
		Transform_t3275118058 * L_37 = GameObject_get_transform_m3490276752(L_36, /*hidden argument*/NULL);
		// tmpPos = new Vector3 (posX,box1.transform.position.y, box1.transform.position.z);
		Vector3_t2243707580  L_38 = Transform_get_position_m2304215762(L_37, /*hidden argument*/NULL);
		V_9 = L_38;
		float L_39 = (&V_9)->get_z_3();
		// tmpPos = new Vector3 (posX,box1.transform.position.y, box1.transform.position.z);
		Vector3__ctor_m1555724485((&V_4), L_31, L_35, L_39, /*hidden argument*/NULL);
		// box1.transform.position = tmpPos;
		GameObject_t1756533147 * L_40 = __this->get_box1_7();
		// box1.transform.position = tmpPos;
		Transform_t3275118058 * L_41 = GameObject_get_transform_m3490276752(L_40, /*hidden argument*/NULL);
		Vector3_t2243707580  L_42 = V_4;
		// box1.transform.position = tmpPos;
		Transform_set_position_m2942701431(L_41, L_42, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void scoreFinal::.ctor()
extern "C"  void scoreFinal__ctor_m203903347 (scoreFinal_t4052325966 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void scoreFinal::Start()
extern "C"  void scoreFinal_Start_m2515337775 (scoreFinal_t4052325966 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (scoreFinal_Start_m2515337775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		// GameObject singleton = GameObject.FindWithTag("singleton");
		// GameObject singleton = GameObject.FindWithTag("singleton");
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m3831463693(NULL /*static, unused*/, _stringLiteral2462408499, /*hidden argument*/NULL);
		V_0 = L_0;
		// singleton.GetComponent<singleton>().getScore();
		GameObject_t1756533147 * L_1 = V_0;
		// singleton.GetComponent<singleton>().getScore();
		singleton_t1734329981 * L_2 = GameObject_GetComponent_Tissingleton_t1734329981_m4058690648(L_1, /*hidden argument*/GameObject_GetComponent_Tissingleton_t1734329981_m4058690648_RuntimeMethod_var);
		// singleton.GetComponent<singleton>().getScore();
		singleton_getScore_m3135182712(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void scoreFinal::Update()
extern "C"  void scoreFinal_Update_m3299016252 (scoreFinal_t4052325966 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void singleton::.ctor()
extern "C"  void singleton__ctor_m4220926752 (singleton_t1734329981 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// singleton singleton::get_Instance()
extern "C"  singleton_t1734329981 * singleton_get_Instance_m3855149394 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (singleton_get_Instance_m3855149394_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	singleton_t1734329981 * V_0 = NULL;
	{
		// return instance;
		IL2CPP_RUNTIME_CLASS_INIT(singleton_t1734329981_il2cpp_TypeInfo_var);
		singleton_t1734329981 * L_0 = ((singleton_t1734329981_StaticFields*)il2cpp_codegen_static_fields_for(singleton_t1734329981_il2cpp_TypeInfo_var))->get_instance_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		// }
		singleton_t1734329981 * L_1 = V_0;
		return L_1;
	}
}
// System.Void singleton::Awake()
extern "C"  void singleton_Awake_m2613295545 (singleton_t1734329981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (singleton_Awake_m2613295545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (instance != null && instance != this)
		IL2CPP_RUNTIME_CLASS_INIT(singleton_t1734329981_il2cpp_TypeInfo_var);
		singleton_t1734329981 * L_0 = ((singleton_t1734329981_StaticFields*)il2cpp_codegen_static_fields_for(singleton_t1734329981_il2cpp_TypeInfo_var))->get_instance_2();
		// if (instance != null && instance != this)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(singleton_t1734329981_il2cpp_TypeInfo_var);
		singleton_t1734329981 * L_2 = ((singleton_t1734329981_StaticFields*)il2cpp_codegen_static_fields_for(singleton_t1734329981_il2cpp_TypeInfo_var))->get_instance_2();
		// if (instance != null && instance != this)
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		// Destroy(this.gameObject);
		// Destroy(this.gameObject);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		// Destroy(this.gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// instance = this;
		IL2CPP_RUNTIME_CLASS_INIT(singleton_t1734329981_il2cpp_TypeInfo_var);
		((singleton_t1734329981_StaticFields*)il2cpp_codegen_static_fields_for(singleton_t1734329981_il2cpp_TypeInfo_var))->set_instance_2(__this);
		// DontDestroyOnLoad(this.gameObject);
		// DontDestroyOnLoad(this.gameObject);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		// DontDestroyOnLoad(this.gameObject);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2658633409(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void singleton::Update()
extern "C"  void singleton_Update_m3601494109 (singleton_t1734329981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (singleton_Update_m3601494109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log(score/2);
		int32_t L_0 = __this->get_score_3();
		int32_t L_1 = ((int32_t)((int32_t)L_0/(int32_t)2));
		RuntimeObject * L_2 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_1);
		// Debug.Log(score/2);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m2923680153(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void singleton::add()
extern "C"  void singleton_add_m1063938385 (singleton_t1734329981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (singleton_add_m1063938385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// score += 1;
		int32_t L_0 = __this->get_score_3();
		__this->set_score_3(((int32_t)((int32_t)L_0+(int32_t)1)));
		// Text text = GameObject.FindWithTag("score").GetComponent<Text>();
		// Text text = GameObject.FindWithTag("score").GetComponent<Text>();
		GameObject_t1756533147 * L_1 = GameObject_FindWithTag_m3831463693(NULL /*static, unused*/, _stringLiteral2247021248, /*hidden argument*/NULL);
		// Text text = GameObject.FindWithTag("score").GetComponent<Text>();
		Text_t356221433 * L_2 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		V_0 = L_2;
		// text.text = (score/2).ToString();
		Text_t356221433 * L_3 = V_0;
		int32_t L_4 = __this->get_score_3();
		V_1 = ((int32_t)((int32_t)L_4/(int32_t)2));
		// text.text = (score/2).ToString();
		String_t* L_5 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		// text.text = (score/2).ToString();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_5);
		// }
		return;
	}
}
// System.Void singleton::getScore()
extern "C"  void singleton_getScore_m3135182712 (singleton_t1734329981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (singleton_getScore_m3135182712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// Text textGame = GameObject.FindWithTag("scoreEnd").GetComponent<Text>();
		// Text textGame = GameObject.FindWithTag("scoreEnd").GetComponent<Text>();
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m3831463693(NULL /*static, unused*/, _stringLiteral3293534843, /*hidden argument*/NULL);
		// Text textGame = GameObject.FindWithTag("scoreEnd").GetComponent<Text>();
		Text_t356221433 * L_1 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_RuntimeMethod_var);
		V_0 = L_1;
		// textGame.text = (score/2).ToString();
		Text_t356221433 * L_2 = V_0;
		int32_t L_3 = __this->get_score_3();
		V_1 = ((int32_t)((int32_t)L_3/(int32_t)2));
		// textGame.text = (score/2).ToString();
		String_t* L_4 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		// textGame.text = (score/2).ToString();
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		// }
		return;
	}
}
// System.Void singleton::.cctor()
extern "C"  void singleton__cctor_m1916394351 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (singleton__cctor_m1916394351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static singleton instance = null;
		((singleton_t1734329981_StaticFields*)il2cpp_codegen_static_fields_for(singleton_t1734329981_il2cpp_TypeInfo_var))->set_instance_2((singleton_t1734329981 *)NULL);
		return;
	}
}
// System.Void touchAction::.ctor()
extern "C"  void touchAction__ctor_m8703832 (touchAction_t352115121 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void touchAction::Start()
extern "C"  void touchAction_Start_m2276556088 (touchAction_t352115121 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void touchAction::Update()
extern "C"  void touchAction_Update_m2574958869 (touchAction_t352115121 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (touchAction_Update_m2574958869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(Input.GetKeyDown("space")){
		// if(Input.GetKeyDown("space")){
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m3563899393(NULL /*static, unused*/, _stringLiteral1144830560, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		// GetComponent<Rigidbody2D>().velocity = new Vector2(0, 5);
		// GetComponent<Rigidbody2D>().velocity = new Vector2(0, 5);
		Rigidbody2D_t502193897 * L_1 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_RuntimeMethod_var);
		// GetComponent<Rigidbody2D>().velocity = new Vector2(0, 5);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m847450945((&L_2), (0.0f), (5.0f), /*hidden argument*/NULL);
		// GetComponent<Rigidbody2D>().velocity = new Vector2(0, 5);
		Rigidbody2D_set_velocity_m2161463521(L_1, L_2, /*hidden argument*/NULL);
	}

IL_002c:
	{
		// }
		return;
	}
}
// System.Void wait2Sec::.ctor()
extern "C"  void wait2Sec__ctor_m1491866603 (wait2Sec_t1458947030 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void wait2Sec::Start()
extern "C"  void wait2Sec_Start_m2322919031 (wait2Sec_t1458947030 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void wait2Sec::Update()
extern "C"  void wait2Sec_Update_m1559094564 (wait2Sec_t1458947030 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (wait2Sec_Update_m1559094564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Invoke("goScene2",2.0f);
		// Invoke("goScene2",2.0f);
		MonoBehaviour_Invoke_m2201735803(__this, _stringLiteral4038778778, (2.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void wait2Sec::goScene2()
extern "C"  void wait2Sec_goScene2_m397741991 (wait2Sec_t1458947030 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (wait2Sec_goScene2_m397741991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SceneManager.LoadScene("scene2-Menu"); }
		// SceneManager.LoadScene("scene2-Menu"); }
		SceneManager_LoadScene_m2357316016(NULL /*static, unused*/, _stringLiteral275778952, /*hidden argument*/NULL);
		// SceneManager.LoadScene("scene2-Menu"); }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
